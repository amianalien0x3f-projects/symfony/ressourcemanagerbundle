<?php

namespace amianalien0x3f\RessourceManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="resmgmt_resource")
 * @ORM\Entity(repositoryClass="amianalien0x3f\RessourceManagerBundle\Repository\RessourceRepository")
 */
class Ressource
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(name="uid",type="string",length=512,nullable=false)
     */
    private $uid;
    /**
     * @ORM\Column(name="name", type="string", length=512, nullable=false)
     */
    private $name;
    /**
     * @ORM\Column(name="type", type="string", length=32, nullable=true)
     */
    private $type;
    /**
     * @ORM\Column(name="creation", type="datetimetz", nullable=true)
     */
    private $creation;
    /**
     * @ORM\Column(name="creator", type="string", length=512, nullable=true)
     */
    private $creator;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uid
     *
     * @param string $uid
     *
     * @return Ressource
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Ressource
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Ressource
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     *
     * @return Ressource
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime
     */
    public function getCreation()
    {
        return $this->creation;
    }

    /**
     * Set creator
     *
     * @param string $creator
     *
     * @return Ressource
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return string
     */
    public function getCreator()
    {
        return $this->creator;
    }
}
