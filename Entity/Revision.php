<?php

namespace amianalien0x3f\RessourceManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="resmgmt_revision")
 * @ORM\Entity(repositoryClass="amianalien0x3f\RessourceManagerBundle\Repository\RevisionRepository")
 */
class Revision
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(name="uid",type="string",length=512,nullable=false)
     */
    private $uid;
    /**
     * @ORM\Column(name="name", type="string", length=512, nullable=false)
     */
    private $name;
    /**
     * @ORM\Column(name="type", type="string", length=32, nullable=true)
     */
    private $type;
    /**
     * @ORM\Column(name="modification", type="datetimetz", nullable=true)
     */
    private $modification;
    /**
     * @ORM\Column(name="modificator", type="string", length=512, nullable=true)
     */
    private $modificator;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uid
     *
     * @param string $uid
     *
     * @return Revision
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Revision
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Revision
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set modification
     *
     * @param \DateTime $modification
     *
     * @return Revision
     */
    public function setModification($modification)
    {
        $this->modification = $modification;

        return $this;
    }

    /**
     * Get modification
     *
     * @return \DateTime
     */
    public function getModification()
    {
        return $this->modification;
    }

    /**
     * Set modificator
     *
     * @param string $modificator
     *
     * @return Revision
     */
    public function setModificator($modificator)
    {
        $this->modificator = $modificator;

        return $this;
    }

    /**
     * Get modificator
     *
     * @return string
     */
    public function getModificator()
    {
        return $this->modificator;
    }
}
