# RessourceManagerBundle
Bundle de gestion de ressource diverses

accès : <url>/resmgmt/{id}?date=DD-MM-YYYY&time=hh:mm:ss
id = Nom litéral d'une ressource
   = identifiant unique d'une ressource
   = identifient unique d'une version de ressource
date = paramètre optionnel afin d'obtenir la ressource dans sa version active à la date donnée, si pas d'heure précisée, prend 23:59:59 (dernière version)
time = paramètre optionnel afin d'obtenir la ressource dans sa version active à l'heure donnée.
Si pas de date précisée, prend la date courante