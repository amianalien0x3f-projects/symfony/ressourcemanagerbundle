<?php

namespace amianalien0x3f\RessourceManagerBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use amianalien0x3f\RessourceManagerBundle\Service\RessourceManagerService;

class DefaultController extends Controller
{   /**
     * @Route("/", name="resmgmt_homepage")
     */
    public function indexAction(Request $request,$activity=null)
    {
        $this->get('resmgmt');
        return $this->render('@amianalien0x3fRessourceManager/homepage.html.twig');
    }
}
